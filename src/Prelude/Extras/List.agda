module Prelude.Extras.List where



open import Prelude

open import Tactic.Reflection.Reright
open import Tactic.Cong

open import Prelude.Extras.Bool
open import Prelude.Extras.Eq
open import Prelude.Variables


list-ind :
  ∀ {ℓ ℓ₁} {A : Set ℓ} {P : List A → Set ℓ₁}
  → P []
  → ((x : A) → (xs : List A) → P xs → P (x ∷ xs))
  → (xs : List A)
  → P xs
list-ind base p [] = base
list-ind base p (x ∷ xs) =
  p x xs (list-ind base p xs)

list-cases :
  ∀ {ℓ ℓ₁} {A : Set ℓ} {P : List A → Set ℓ₁}
  → (l : List A)
  → (P [])
  → ((p : A × List A) → P (fst p ∷ snd p))
  → (P l)
list-cases [] null-case _ = null-case
list-cases (x ∷ xs) _ cons-case = cons-case ( x , xs )

l≮[] : {A : Set ℓ} → {l : List A} → length l < length {A = A} [] → ⊥
l≮[] {A = A} {l = x ∷ xs} (diff k ())

e++[]≡[] : {A : Set ℓ} → (e : List A) → e ++ [] ≡ e
e++[]≡[] = list-ind refl (λ x xs → cong (x ∷_) )
--e++[]≡[] [] = refl
--e++[]≡[] (x ∷ xs) = cong (_ ∷_) (e++[]≡[] xs)


xs++y∷ys≢[] :
  ∀ {ℓ} {A : Set ℓ}
  → (xs : List A)
  → (y : A)
  → (ys : List A)
  → (xs ++ (y ∷ ys)) ≢ []
xs++y∷ys≢[] [] _ _ ()
xs++y∷ys≢[] (x ∷ xs) _ _ ()


xs++[y]≢[] :
  ∀ {ℓ} {A : Set ℓ}
  → (xs : List A)
  → (y : A)
  → (xs ++ [ y ]) ≢ []
xs++[y]≢[] xs y = xs++y∷ys≢[] xs y []


xs++y∷ys≡xs++[y]++ys :
  ∀ {ℓ} {A : Set ℓ}
  → (xs : List A)
  → (y : A)
  → (ys : List A)
  → xs ++ y ∷ ys ≡ xs ++ [ y ] ++ ys
xs++y∷ys≡xs++[y]++ys [] y ys = refl
xs++y∷ys≡xs++[y]++ys (x ∷ xs) y ys =
  cong (_ ∷_) (xs++y∷ys≡xs++[y]++ys xs y ys)


++-commute : ∀ {ℓ} {A : Set ℓ} → (a b c : List A) → (a ++ b) ++ c ≡ a ++ b ++ c
++-commute [] b c = refl
++-commute (a ∷ as) b c =
  cong (a ∷_) (++-commute as b c)

filter-head-true :
  ∀ {a} {A : Set a} {{EQA : Eq A}}
  → (f : A → Bool) → (x : A) → (xs : List A)
  → (f x ≡ true)
  → filter f (x ∷ xs) ≡ x ∷ filter f xs
filter-head-true f x xs fx≡true =
  cong (λ z → if z then (x ∷ filter f xs) else filter f xs)
       fx≡true

filter-head-false :
  ∀ {a} {A : Set a} {{EQA : Eq A}}
  → (f : A → Bool) → (x : A) → (xs : List A)
  → (f x ≡ false)
  → filter f (x ∷ xs) ≡ filter f xs
filter-head-false f x xs fx≡false =
  cong (λ z → if z then (x ∷ filter f xs) else filter f xs)
       fx≡false

filter-++-distr :
  ∀ {a} {A : Set a} {{EQA : Eq A}}
  → (f : A → Bool) → (l : List A) → (r : List A)
  → filter f (l ++ r) ≡ (filter f l ++ filter f r)
filter-++-distr f [] r = refl
filter-++-distr f (l ∷ ls) r
  with inspect (f l)
...| (true , ingraph fl≡true) =
  filter f ((l ∷ ls) ++ r) ≡⟨ refl ⟩
  filter f (l ∷ (ls ++ r)) ≡⟨ filter-head-true f l (ls ++ r) fl≡true ⟩
  l ∷ filter f (ls ++ r) ≡⟨  cong (l ∷_) (filter-++-distr f ls r) ⟩
  l ∷ (filter f ls ++ filter f r) ≡⟨ refl ⟩
  ((l ∷ filter f ls) ++ filter f r) ≡⟨ by-cong (sym (filter-head-true f l ls fl≡true)) ⟩
  (filter f (l ∷ ls) ++ filter f r) ∎
...| (false , ingraph fl≡false) =
  filter f ((l ∷ ls) ++ r) ≡⟨ refl ⟩
  filter f (l ∷ (ls ++ r)) ≡⟨ filter-head-false f l (ls ++ r) fl≡false ⟩
  filter f (ls ++ r) ≡⟨  (filter-++-distr f ls r) ⟩
  (filter f ls ++ filter f r) ≡⟨ by-cong (sym (filter-head-false f l ls fl≡false)) ⟩
  (filter f (l ∷ ls) ++ filter f r) ∎

null-++-distr :
  ∀ {a} {A : Set a}
  → (l : List A) → (r : List A)
  → null (l ++ r) ≡ (null l && null r)
null-++-distr [] r = refl
null-++-distr (l ∷ xs) r =  refl

elem-++-distr :
  ∀ {a} {A : Set a} {{EQA : Eq A}}
  → (x : A) → (l : List A) → (r : List A)
  → elem x (l ++ r) ≡ (elem x l || elem x r)
elem-++-distr x l r =
    elem x (l ++ r)
  ≡⟨ refl ⟩
    not (null (filter (isYes ∘ _==_ x) (l ++ r)))
  ≡⟨ by-cong  (filter-++-distr (isYes ∘ _==_ x) l r) ⟩
    not (null ((filter (isYes ∘ _==_ x) l) ++ (filter (isYes ∘ _==_ x) r)))
  ≡⟨ cong not (null-++-distr (filter (isYes ∘ _==_ x) l)
                              (filter (isYes ∘ _==_ x) r))
    ⟩
    not (null (filter (isYes ∘ _==_ x) l) && null (filter (isYes ∘ _==_ x) r))
  ≡⟨ de-morg-neg-conj (null (filter (isYes ∘ _==_ x) l)) _ ⟩
     (elem x l) || (elem x r)
  ∎

right-append-non-empty-nonempty :
  ∀ {ℓ}
  → {A : Set ℓ }
  → {l r : List A}
  → r ≢ []
  → (l ++ r) ≢ []
right-append-non-empty-nonempty { l = [] } {r = r} r≢[] = r≢[]
right-append-non-empty-nonempty {l = (x ∷ xs)} {r = r} r≢[] ()


-- elem-++ₗ :
--   ∀ {a} {A : Set a} {{EQA : Eq A}}
--   → (l : List A) → (r : List A) → (x : A) → elem x l ≡ true
--   → elem x (l ++ r) ≡ true
-- elem-++ₗ
