module Prelude.Extras.Nat where



open import Prelude
open import Prelude.Extras.Eq

suc-neg : (n m : Nat)
        → n ≡ suc m
        → n ≡ suc (n - 1)
suc-neg n m refl = refl


suc-inj : (n m : Nat) → _≡_  {A = Nat} (suc n) (suc m) → n ≡ m
suc-inj n m refl = refl


-- n<m⇒¬n≡m :
--     ∀ {a b : Nat}
--     → a < b → ¬ (a ≡ b)
-- n<m⇒¬n≡m {a} {b} d@(diff k eq') eq =
--   less-antirefl {x = b} (transport (cong (_< b) eq) d)
