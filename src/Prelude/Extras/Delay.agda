{-# OPTIONS --sized-types #-}

module Prelude.Extras.Delay where

open import Prelude hiding ( force )

open import Prelude.Extras.Size

open import Codata.Delay  public
open import Codata.Thunk using (Thunk; force ) public

open import Size using (∞ ; Size) public

open import Codata.Delay as Delay

open import Relation.Binary.PropositionalEquality using (subst)

import Codata.Conat as Conat

-- Instances for syntax
instance
  FunctorDelay : ∀ {a i} → Functor {a = a} (λ A → Delay A i)
  fmap {{FunctorDelay}} = (λ f a → Delay.map f a)

  ApplicativeDelay : ∀ {a i} → Applicative {a = a} (λ A → Delay A i)
  pure {{ApplicativeDelay}} a = now a
  _<*>_ {{ApplicativeDelay}} df dx =
    Delay.bind df (λ f → Delay.bind dx  (λ x → pure (f x)))

  MonadDelay : ∀ {a i} → Monad {a = a} (λ A → Delay A i)
  _>>=_ {{MonadDelay}} = bind



module _ {p} (P : Size → Set p) where
  open import Relation.Unary
  cofix : ∀[ Thunk P ⇒ P ] → ∀[ P ]
  cofix f = f aux
    module Cofix where
    aux = λ where .force → cofix f



-- Some extra utitilies
instance
  Sized⇓ : ∀ {ℓ} {P : Set ℓ} {x : Delay P ∞} → Sized (x ⇓)
  size {{Sized⇓}} = Conat.toℕ ∘ Delay.length-⇓




-- Tbh, i do not understand this yet, gotta look into subst
extract-subst : ∀ {ℓ} {A : Set ℓ} {x y : Delay A _} (p : x ≡ y) {x⇓ : x ⇓} → Delay.extract (subst _⇓ p x⇓) ≡ Delay.extract x⇓
extract-subst refl = refl

-- If we have two proofs of termination of the same element, then the
-- terimination proofs are equivalent
⇓-unique :
  ∀ {a} → {A : Set a}
  → {d : Delay A ∞}
  → (d⇓₁ : d ⇓)
  → (d⇓₂ : d ⇓)
  → d⇓₁ ≡ d⇓₂
⇓-unique {d = now s} (now s) (now s) = refl
⇓-unique {d = later d'} (later l) (later r) =
  cong later (⇓-unique {d = force d'} l r)



-- If a bind of two delays terminates, then so do the first delay
bind-⇓-injₗ :
  ∀ {a} {A B : Set a}
  → {d : Delay A ∞} {f : A → Delay B ∞}
  → Delay.bind d f ⇓ → d ⇓
bind-⇓-injₗ {d = now s} foo = now s
bind-⇓-injₗ {d = later s} (later foo) =
  later (bind-⇓-injₗ foo)

-- If a bind between two delays termiantes then so do the function applied with
-- the result of the first
bind-⇓-injᵣ :
  ∀ {a} {A B : Set a}
  → {d : Delay A ∞} {f : A → Delay B ∞}
  → (bind⇓ : Delay.bind d f ⇓)
  → f (Delay.extract (bind-⇓-injₗ {d = d} {f = f} bind⇓)) ⇓
bind-⇓-injᵣ {d = now s} foo = foo
bind-⇓-injᵣ {d = later s} {f} (later foo) =
  (bind-⇓-injᵣ {d = force s} {f = f} foo)




-- The extracted value of a bind is equivalent to the extracted value of its
-- second element
extract-bind :
  ∀ {a} → {A B : Set a}
  → {m : Delay A Size.∞} → {f : A → Delay B Size.∞}
  → (m⇓ : m ⇓) → (f⇓ : ((f (Delay.extract m⇓)) ⇓))
  → Delay.extract (Delay.bind-⇓ m⇓ {f} f⇓) ≡ Delay.extract f⇓
extract-bind (now a) f⇓ = refl
extract-bind (later t) f⇓ = extract-bind t f⇓

-- If the right element of a bind returns a certain value so does the entire
-- bind
extract-bind-⇓-injᵣ≡extract-bind-⇓ :
  ∀ {a} {A B : Set a}
  → {d : Delay A ∞} {f : A → Delay B ∞}
  → (bind⇓ : bind d f ⇓)
  → extract (bind-⇓-injᵣ {d = d} bind⇓) ≡ extract bind⇓
extract-bind-⇓-injᵣ≡extract-bind-⇓ {d = now s} bind⇓ = refl
extract-bind-⇓-injᵣ≡extract-bind-⇓ {d = later s} {f} (later bind⇓) =
  extract-bind-⇓-injᵣ≡extract-bind-⇓ {d = force s} {f = f} bind⇓

extract-now :
  ∀ {a} → {A : Set a} → {s : A} → (s⇓ : now s ⇓)
  → extract s⇓ ≡ s
extract-now {s = s} s⇓@(now s') = refl


bind-now :
    ∀ {a} {A B : Set a}
    → {d : A} {f : A → Delay B ∞}
    → (bind⇓ : (Delay.bind (now d) f ⇓))
    → (f⇓ : (f d ⇓))
    → bind⇓ ≡ f⇓
bind-now = ⇓-unique

delay-drop : ∀ {a} {A : Set a}
           → Delay A ∞
           → Delay A ∞
delay-drop (now x) = now x
delay-drop (later t) = force t

drop-⇓ : ∀ {a} {A : Set a}
        → {d : Delay A ∞}
        → (p : d ⇓)
        → (delay-drop d) ⇓
drop-⇓ (now x) = now x
drop-⇓ (later t) = t

bind-later-suc :
    ∀ {a} {A B : Set a}
    → {d : Delay A ∞} {f : A → Delay B ∞}
    → {dt : Thunk (Delay A) ∞}
    → (d⇓ : later dt ⇓)
    → (bind⇓ : (Delay.bind (later dt) f ⇓))
    → (f⇓ : (f (extract d⇓) ⇓))
    → Conat.toℕ (length-⇓ bind⇓) ≡ suc (Conat.toℕ (length-⇓ (drop-⇓ bind⇓)))
bind-later-suc d⇓ (later bind⇓) f⇓ = refl

bind-later-suc-sigma :
    ∀ {a} {A B : Set a}
    → {d : Delay A ∞} {f : A → Delay B ∞}
    → {dt : Thunk (Delay A) ∞}
    → (d⇓ : later dt ⇓)
    → (bind⇓ : (Delay.bind (later dt) f ⇓))
    → (f⇓ : (f (extract d⇓) ⇓))
    → Σ (Delay A ∞)
      (λ d' → Σ (Delay.bind d' f ⇓)
         (λ bind⇓' → ((d' ⇓) × size bind⇓ ≡ suc (size bind⇓'))))
bind-later-suc-sigma {d = d} {dt = dt} d⇓@(later d'⇓) (later bind⇓) f⇓ =
  (force dt) , bind⇓ , d'⇓  , refl


-- Proof that the length of a proof that a bind terminates is equal to the sum
-- of the length that its components terminates
bind-⇓-lenght-add :
    ∀ {a} {A B : Set a}
    → {d : Delay A ∞} {f : A → Delay B ∞}
    → (bind⇓ : Delay.bind d f ⇓)
    → (d⇓ : d ⇓)
    → (f⇓ : f (Delay.extract d⇓) ⇓)
    → size bind⇓ ≡ size d⇓ + size f⇓
bind-⇓-lenght-add {f = f} bind⇓ d⇓@(now s') f⇓ =
    size bind⇓
  ≡⟨ cong size (bind-now {d = s'} {f = f} bind⇓ f⇓) ⟩
    size f⇓
  ≡⟨ refl ⟩
    size (now s')  + size f⇓
  ∎
bind-⇓-lenght-add {d = d@(later dt)} {f = f} bind⇓@(later bind'⇓) d⇓@(later r) f⇓ =
    size bind⇓
  ≡⟨ refl ⟩
    suc (size bind'⇓)
  ≡⟨ cong suc (bind-⇓-lenght-add bind'⇓ r f⇓) ⟩
    suc (size r) + size f⇓
  ≡⟨ refl ⟩
    size d⇓  + size f⇓
  ∎


-- size-x⇓≡sucn :
--   ∀ {a} {A : Set a}
--   → {d : Delay A ∞}
--   → (d⇓ : d ⇓)
--   → Σ Nat (λ n → size d⇓ ≡ suc n)
-- size-x⇓≡sucn (now _) = (0 , refl)
-- size-x⇓≡sucn (later _) = ?
