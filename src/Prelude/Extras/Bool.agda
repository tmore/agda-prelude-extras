module Prelude.Extras.Bool where

open import Prelude
open import Prelude.Variables

open import Prelude.Extras.Eq

open import Level using (0ℓ)


de-morg-neg-conj : (a : Bool) → (b : Bool) → not (a && b) ≡ (not a || not b)
de-morg-neg-conj true true = refl
de-morg-neg-conj false true = refl
de-morg-neg-conj true false  = refl
de-morg-neg-conj false false  = refl

de-morg-neg-disj : (a : Bool) → (b : Bool) → not (a || b) ≡ (not a && not b)
de-morg-neg-disj true true = refl
de-morg-neg-disj false true = refl
de-morg-neg-disj true false  = refl
de-morg-neg-disj false false  = refl

x||true : {x : Bool} → (x || true) ≡ true
x||true {true} = refl
x||true {false} = refl


-- This should be solved automatically?
true≢false : _≢_ { 0ℓ } {Bool} true false
true≢false ()

false≢true : _≢_ { 0ℓ } {Bool} false true
false≢true ()


IsTrue[x]⇒x≡true : {x : Bool} → IsTrue x → x ≡ true
IsTrue[x]⇒x≡true true = refl

IsFalse[x]⇒x≡false : {x : Bool} → IsFalse x → x ≡ false
IsFalse[x]⇒x≡false false = refl



not[x]≡false⇒x≡true : {x : Bool}
                      → not x ≡ false
                      → x ≡ true
not[x]≡false⇒x≡true {true} _ = refl
not[x]≡false⇒x≡true {false} ()


not[x]≡true⇒x≡false : {x : Bool}
                       → not x ≡ true
                       → x ≡ false
not[x]≡true⇒x≡false {true} ()
not[x]≡true⇒x≡false {false} _ = refl
