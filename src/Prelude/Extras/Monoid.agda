module Prelude.Extras.Monoid where

open import Prelude
open import Prelude.Extras.List



record Monoid/Laws {ℓ} (A : Set ℓ) : Set ℓ where
  field
    overlap {{super}} : Monoid A
    left-ident : (e : A) → mempty <> e ≡ e
    right-ident : (e : A) → e <> mempty ≡ e
    monoid-comm : (a b c : A) → (a <> b) <> c ≡ a <> (b <> c)

open Monoid/Laws {{...}} public hiding (super)

instance
  MonoidLawsList : ∀ {ℓ} {A : Set ℓ} → Monoid/Laws (List A)
  Monoid/Laws.super MonoidLawsList = it
  left-ident {{MonoidLawsList}} e = refl
  right-ident {{MonoidLawsList}} e = e++[]≡[] e
  monoid-comm {{MonoidLawsList}} =  ++-commute
--  less-trans    {{OrdLawsBool}} false<true ()
