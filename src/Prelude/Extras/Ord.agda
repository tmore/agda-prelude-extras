module Prelude.Extras.Ord where

open import Prelude


open import Prelude.Variables

open import Prelude.Extras.Eq
open import Prelude.Extras.Bool



_≮_ : {A : Set ℓ} {{_ : Ord A}} (a b : A) → Set ℓ
a ≮ b = ¬ (a < b)

_≯_ : {A : Set ℓ} {{_ : Ord A}} (a b : A) → Set ℓ
_≯_ = flip _≮_

_≰_ : {A : Set ℓ} {{_ : Ord A}} (a b : A) → Set ℓ
a ≰ b = ¬ (a ≤ b)

_≱_ : {A : Set ℓ} {{_ : Ord A}} (a b : A) → Set ℓ
_≱_ = flip _≰_


<⇒≢ : ∀ {ℓ} {A : Set ℓ} {{Ord/LawsA : Ord/Laws A}} {a b : A} → a < b → a ≢ b
<⇒≢ a<b a≡b rewrite a≡b = less-antirefl a<b

-- Ord/Laws extra lemmas
module _  {A : Set ℓ} {{Ord/LawsA : Ord/Laws A}} where

  <⇒≤ : {a b : A} → a < b → a ≤ b
  <⇒≤ = lt-to-leq

  ≡⇒≮ : {a b : A} → a ≡ b → a ≮ b
  ≡⇒≮ refl a<b = less-antisym a<b a<b

  <⇒≱ : {a b : A} → a < b → a ≱ b
  <⇒≱ = flip leq-less-antisym



  ≮⇒≥ : {a b : A} → a ≮ b → a ≥ b
  ≮⇒≥ {a = a} {b = b} ¬a<b
    with compare a b
  ...| less a<b = ⊥-elim (¬a<b a<b)
  ...| equal a≡b = eq-to-leq (sym a≡b)
  ...| greater a>b = lt-to-leq a>b

  ≰⇒> : {a b : A} → a ≰ b → a > b
  ≰⇒> {a = a} {b = b} a≰b
    with compare a b
  ...| less a<b = ⊥-elim (a≰b (lt-to-leq a<b))
  ...| equal a≡b = ⊥-elim (a≰b (eq-to-leq a≡b))
  ...| greater b>a = b>a

  ≰⇒≥ : {a b : A} → a ≰ b → a ≥ b
  ≰⇒≥ = <⇒≤ ∘ ≰⇒>

  infix 4 _<-dec_ _≤-dec_
  _≤-dec_ : (a b : A) → Dec (a ≤ b)
  _≤-dec_ a b
    with compare a b
  ...| less a<b = yes (lt-to-leq a<b)
  ...| equal a≡b = yes (eq-to-leq a≡b)
  ...| greater a>b = no (λ x → leq-less-antisym x a>b)

  _<-dec_ : (a b : A) → Dec (a < b)
  _<-dec_ a b
    with compare a b
  ...| less a<b = yes a<b
  ...| equal a≡b rewrite a≡b = no less-antirefl
  ...| greater a>b = no (λ x → less-antisym x a>b)



  <?⇒< :  {a b : A} → (a <? b) ≡ true → a < b
  <?⇒< {a = a} {b = b} a<?b
    with compare a b
  ...| less a<b = a<b
  ...| equal a≡b rewrite a≡b =
    ⊥-elim (false≢true a<?b)
  ...| greater _ = ⊥-elim (false≢true  a<?b)

  ≮?⇒≮ : {a b : A} → (a <? b) ≡ false → a ≮ b
  ≮?⇒≮ {a = a} {b = b} a<?b≡false
    with compare a b
  ...| less a<b = λ x → true≢false a<?b≡false
  ...| equal a≡b rewrite a≡b = λ a<b → ⊥-elim (less-antirefl a<b)
  ...| greater a>b = λ a<b → ⊥-elim (less-antisym a>b a<b)

  ≤?⇒≤ :  {a b : A} → (a ≤? b) ≡ true → a ≤ b
  ≤?⇒≤ {a = a} {b = b} a≤?b
    with inspect (compare b a)
  ...| less a<b , ingraph compare≡ rewrite compare≡ =
       ⊥-elim (false≢true a≤?b)
  ...| equal a≡b , _ = eq-to-leq (sym a≡b)
  ...| greater a>b , _ = lt-to-leq a>b

  ≰?⇒≰ :  {a b : A} → (a ≤? b) ≡ false → a ≰ b
  ≰?⇒≰ {a = a} {b = b} a≰?b
    with (compare b a)
  ...| less b<a =
       <⇒≱ b<a
  ...| equal a≡b rewrite a≡b = ⊥-elim (true≢false a≰?b)
  ...| greater a>b = ⊥-elim (true≢false a≰?b)

  ≰?⇒≥ : {a b : A} → (a ≤? b) ≡ false → a ≥ b
  ≰?⇒≥ = ≰⇒≥ ∘ ≰?⇒≰
n≮0 : {n : Nat} → n < 0 → ⊥
n≮0 (diff k ())
