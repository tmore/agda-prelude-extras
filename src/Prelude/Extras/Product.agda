module Prelude.Extras.Product where

open import Prelude

pair-inj :
  ∀ {ℓ ℓ₁} {A : Set ℓ} {B : Set ℓ₁} {a₁ a₂ : A} → {b₁ b₂ : B} → ((a₁ , b₁) ≡ (a₂ , b₂)) → (a₁ ≡ a₂ × b₁ ≡ b₂)
pair-inj refl = refl , refl

-- Special case of pair-inj for siplicity
tripple-inj :
  ∀ {ℓ ℓ₁ ℓ₂} {A : Set ℓ} {B : Set ℓ₁} {C : Set ℓ₂}
  → {a₁ a₂ : A} → {b₁ b₂ : B} → {c₁ c₂ : C}
  → ((a₁ , b₁ , c₁) ≡ (a₂ , b₂ , c₂)) → (a₁ ≡ a₂ × b₁ ≡ b₂ × c₁ ≡ c₂)
tripple-inj refl = refl , refl , refl

pair-elems-eq-pair-eq :
  ∀ {ℓ₁ ℓ₂} → {A : Set ℓ₁} {B : Set ℓ₂}
  → {a a' : A} {b b' : B}
  → a ≡ a'
  → b ≡ b'
  → (a , b) ≡ (a' , b')
pair-elems-eq-pair-eq refl refl = refl
