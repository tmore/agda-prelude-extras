module Prelude.Extras.Int where


open import Prelude
open import Prelude.Int.Properties
open import Prelude.Nat.Properties

open import Prelude.Extras.Ord
import Prelude.Nat.Properties as Nat



neg≱pos : (n m : Nat) → (negsuc n) ≥ (pos m) → ⊥
neg≱pos zero zero (diff k ())
neg≱pos zero (suc m) (diff k ())
neg≱pos (suc n) zero (diff k ())
neg≱pos (suc n) (suc m) (diff k ())


int-mul-one-r : (n : Int) → n * 1 ≡ n
int-mul-one-r (pos n) = cong pos (Nat.mul-one-r n)
int-mul-one-r (negsuc n) = cong negsuc (Nat.mul-one-r n)

negsuc<pos :  (n m : Nat) → (negsuc n) < (pos m)
negsuc<pos n m with compareInt (negsuc n) (pos m)
... | less lt = lt
... | greater gt = ⊥-elim $ neg≱pos n m (<⇒≤ gt)



int-suc : (n : Int) → (n > 0) → Σ Nat (λ m → n ≡ pos (suc m))
int-suc (pos 0) (diff k ())
int-suc (pos (suc n)) _ = (n , refl)
int-suc n'@(negsuc n) (diff k ())

--1-NZ-suc : (n : Nat) → 1 -NZ suc n ≡ negsuc n
--1-NZ-suc 0 = refl
--1-NZ-suc (suc n) = {!!}

lt-sucInt : (a b : Int) → (a < b) → a < (1 + b)
lt-sucInt (pos a) (pos b) (diff k eq)
  rewrite pos-inj eq
  = diff (suc k) (cong pos auto)
  where
    open import Tactic.Nat
lt-sucInt (negsuc n) (pos n₁) (diff k eq) =
    diff (suc k)
         (pos (suc n₁)
         ≡⟨ cong (1 +_) (trans eq (-NZ-spec (suc k) (suc n))) ⟩
           1 + diffNat (suc k) (suc n)
         ≡⟨ addInt-pos 1 _ ⟩
           sucInt (diffNat (suc k) (suc n))
         ≡⟨ sym (diffNat-suc-l (suc k) (suc n)) ⟩
           diffNat (suc (suc k)) (suc n)
         ≡⟨ sym  (-NZ-spec (suc (suc k)) (suc n)) ⟩
           (suc (suc k) -NZ suc n)
          ∎
         )
lt-sucInt (negsuc a) (negsuc 0) (diff k eq) =
  negsuc<pos a 0
lt-sucInt (negsuc a) (negsuc (suc b)) (diff k eq)
  = diff (suc k) (negsuc b
                 ≡⟨ cong sucInt eq ⟩
                   sucInt ((suc k) -NZ suc a)
                 ≡⟨ sym (-NZ-suc-l (suc k) (suc a)) ⟩
                   suc (suc k) -NZ suc a
                 ∎)



lt-predIntᵣ : (a b : Int) → (a < (b - 1)) → a < b
lt-predIntᵣ a b a<b-1 =
  transport (λ z → (a < z))
            ((cong (1 +_) (addInt-commute b (-1)))
             ⟨≡⟩ (addInt-assoc 1 -1 b)
             ⟨≡⟩ addInt-zero-l b)
            (lt-sucInt a (b - 1) a<b-1)

lt-predIntₗ :  (a b : Int) → (a < b) → (a - 1) < b
lt-predIntₗ (pos n) (pos n₁) (diff k eq)
  rewrite eq =
  diff (suc k)
       (trans (sym (addInt-assoc (pos (suc (suc k))) -1 (pos n)))
              (cong (pos (suc (suc k)) +_) (addInt-commute -1 (pos n))))
lt-predIntₗ (negsuc n) (pos n₁) (diff k eq)
  rewrite eq
  rewrite add-zero-r (suc n)
  rewrite -NZ-spec (suc k) (suc n)
  rewrite sym (-NZ-spec (suc (suc k)) (suc (suc n))) =
  diff (suc k) refl
lt-predIntₗ (negsuc n) (negsuc n₁) (diff k eq)
  rewrite eq
  rewrite add-zero-r (suc n)
  rewrite -NZ-spec (suc k) (suc n)
  rewrite sym (-NZ-spec (suc (suc k)) (suc (suc n))) =
  diff (suc k) refl

pos*neg : (a b : Nat) → pos a * neg b ≡ neg (a * b)
pos*neg a 0 rewrite mul-zero-r a = refl
pos*neg a (suc b) = refl

negsuc*neg : (a b : Nat) → negsuc a * neg b ≡ pos (suc a * b)
negsuc*neg a 0 rewrite mul-zero-r a = refl
negsuc*neg a (suc b) = refl

mulInt-commute : (a b : Int) → a * b ≡ b * a
mulInt-commute (pos a) (pos b) = cong pos (mul-commute a b)
mulInt-commute (pos a) (negsuc b) = cong neg (mul-commute a (suc b))
mulInt-commute (negsuc a) (pos b) = cong neg (mul-commute (suc a) b)
mulInt-commute (negsuc a) (negsuc b) = cong pos (mul-commute (suc a) (suc b))

mulInt-assoc : (a b c : Int) → a * (b * c) ≡ a * b * c
mulInt-assoc (pos a) (pos b) (pos c) =
  cong pos (mul-assoc a b c)
mulInt-assoc (pos a) (pos b) (negsuc c)
  rewrite pos*neg a (b * suc c) =
  cong neg (mul-assoc a b (suc c))
mulInt-assoc (pos a) (negsuc b) (pos c)
  rewrite pos*neg a (suc b * c)
  rewrite mulInt-commute (neg (a * suc b)) (pos c)
  rewrite pos*neg c (a * (suc b))
  =
  cong neg (trans (mul-assoc a (suc b) c)
                  (mul-commute (a * suc b) c)
                  )
mulInt-assoc (pos a) (negsuc b) (negsuc c)
  rewrite mulInt-commute (neg (a * suc b)) (negsuc c)
  rewrite negsuc*neg c (a * suc b)
  rewrite mul-commute (suc c) (a * suc b)
  = cong pos (mul-assoc a (suc b) (suc c))
mulInt-assoc (negsuc a) (pos b) (pos c)
  rewrite mulInt-commute (neg (suc a * b)) (pos c)
  rewrite pos*neg c (suc a * b)
  rewrite mul-commute c  (suc a * b)
  = cong neg (mul-assoc (suc a) b c)
mulInt-assoc (negsuc a) (pos b) (negsuc c)
  rewrite negsuc*neg a (b * suc c)
  rewrite mulInt-commute (neg (suc a * b)) (negsuc c)
  rewrite negsuc*neg c (suc a * b)
  rewrite mul-commute (suc c) (suc a * b)
  = cong pos (mul-assoc (suc a) b (suc c))
mulInt-assoc (negsuc a) (negsuc b) (pos c)
  rewrite negsuc*neg a (suc b * c)
  = cong pos (mul-assoc (suc a) (suc b) c)
mulInt-assoc (negsuc a) (negsuc b) (negsuc c) =
   cong neg (mul-assoc (suc a) (suc b) (suc c))
