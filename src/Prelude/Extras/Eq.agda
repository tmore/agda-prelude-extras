module Prelude.Extras.Eq where

open import Prelude
open import Prelude.Variables

_≢_ : (a b : A) → Set _
a ≢ b = ¬ (a ≡ b)

≢-sym : {a b : A} → a ≢ b → b ≢ a
≢-sym a≢b refl = a≢b refl

module _ {ℓ : Level} {A : Set ℓ} {{EqA : Eq A}} where



  _==?_ : A → A → Bool
  _==?_ a b = isYes (a == b)

  ==-reflexive : (x : A) → (x == x) ≡ yes refl
  ==-reflexive x with  x == x
  ...| yes refl = refl
  ...| no notEq = ⊥-elim (notEq refl)

  ==?-reflexive : (x : A) → (x ==? x) ≡ true
  ==?-reflexive x with  x == x
  ...| yes refl = refl
  ...| no notEq = ⊥-elim (notEq refl)



  ¬sym : {a b : A} → ¬ (a ≡ b) → ¬ (b ≡ a)
  ¬sym ¬eq = (λ eq → (¬eq (sym eq)))

  ≡⇒==? : {a b : A} → (a ≡ b) → (a ==? b) ≡ true
  ≡⇒==? {a = a} {b = b} refl with a == b
  ...| yes refl = refl
  ...| no ¬eq = ⊥-elim (¬eq refl)



  ≢⇒==? : {a b : A} → a ≢ b → isYes (a == b) ≡ false
  ≢⇒==? {a = a} {b = b} ¬eq with a == b
  ...| yes eq = ⊥-elim (¬eq eq)
  ...| no ¬eq' = refl

  a==?b⇒a≡b : {a b : A} → (a ==? b) ≡ true → a ≡ b
  a==?b⇒a≡b {a = a} {b = b} a==?b≡true
    with a == b
  ...| yes eq = eq
  ...| no ¬eq with (≢⇒==? ¬eq) | a==?b≡true
  ...| _ | ()


  ==?≡false⇒≢ : {a b : A} → (a ==? b) ≡ false → a ≢ b
  ==?≡false⇒≢ {a = a} {b = b} ==?≡false
    with a == b
  ...| no ¬eq = ¬eq
  ...| yes eq with ≡⇒==? | ==?≡false
  ...| _ | ()
