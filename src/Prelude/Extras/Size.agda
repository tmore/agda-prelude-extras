module Prelude.Extras.Size where

open import Prelude


record Sized {ℓ} (A : Set ℓ) : Set ℓ where
  field
    size : A → Nat
open Sized {{...}} public


instance
  SizedNat : Sized Nat
  size {{SizedNat}} = id

instance
  SizedList : ∀ {ℓ} {A : Set ℓ} → Sized (List A)
  size {{SizedList}} = length
