module Prelude.Extras where


open import Prelude.Extras.Eq public
open import Prelude.Extras.Ord public
open import Prelude.Extras.List public
open import Prelude.Extras.Product public
open import Prelude.Extras.Nat public
open import Prelude.Extras.Int public
open import Prelude.Extras.Bool public
open import Prelude.Extras.Monoid public
open import Prelude.Extras.Size public
